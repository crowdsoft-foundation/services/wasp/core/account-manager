##
import requests, json, os
import datetime
import shutil
from planblick import autorun, sqs
from db.account import Account
from db.logins import Logins



class Autorun(autorun.Autorun):

    def __init__(self):
        super(Autorun, self).__init__()

    def post_deployment(self):
        print("CREATING TABLE ACCOUNTS FOR ACCOUNT-MANAGER")
        Account()
        print("CREATING TABLE LOGINS FOR ACCOUNT-MANAGER")
        Logins()

        request = requests.get(self.kong_api)
        if request.status_code == 200:
            self.add_service("Account-Manager", "http://account-manager.planblick.svc")
            self.add_route(service_name="Account-Manager", path="/login", plugins=["basic-auth"], strip_path=False)
            self.add_route(service_name="Account-Manager", path="/logout", strip_path=False)
            self.add_route(service_name="Account-Manager", path="/get_users", strip_path=False)
            self.add_route(service_name="Account-Manager", path="/registration", plugins=[], strip_path=False)
            self.add_route(service_name="Account-Manager", path="/checklogin", plugins=[], strip_path=False)
        else:
            print("Kong-API not available:", self.kong_api)



Autorun().post_deployment()

