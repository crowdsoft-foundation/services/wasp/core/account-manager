from config import Config
from sqlalchemy import Column, DateTime, Integer, String, Text, Sequence, Boolean
from db.abstract import DbEntity
from sqlalchemy.ext.declarative import declarative_base
from pbglobal.pblib.amqp import client
import copy
import datetime
import json


Base = declarative_base()
config = Config()


class Account(DbEntity, Base):
    __tablename__ = 'accounts'
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8mb4', 'mysql_collate': 'utf8mb4_general_ci'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    account_id = Column(String(255), index=True)
    email = Column(String(255))
    creator = Column(String(255))
    owner = Column(String(255))
    password = Column(String(5000))
    confirmed = Column(Boolean, default=False)
    data = Column(Text)
    correlation_id = Column(String(255))
    version = Column(Integer)
    creationTime = Column(DateTime,  default=datetime.datetime.utcnow)
    login = Column(String(255))
    profile_data = Column(Text)

    def __init__(self):
        super().__init__()
        Base.metadata.create_all(Config.db_engine)

    def __repr__(self):
        return "<Account(id='%s', version='%s' email='%s', confirmed='%s',  correlation_id='%s', )>\r\n%s\r\n----\r\n%s" % (
            self.id, self.version, self.email, self.confirmed, self.correlation_id, self.data, self.profile_data)

    def save(self):
        self.fire()

    def commit(self):
        raise Exception("For events, usage of commit and add functions are forbidden. Either use save() oder fire()")

    def add(self):
        raise Exception("For events, usage of commit and add functions are forbidden. Either use save() oder fire()")

    def rollback(self):
        raise Exception("For events, usage of rollback is forbidden.")

    def fire(self):
        message = copy.deepcopy(self.__dict__)
        if "_sa_instance_state" in message:
            del message["_sa_instance_state"]
        try:
            self.session.add(self)
            self.session.commit()
            # queue_connector = client()
            # queue_connector.publish(toExchange="message_handler", routingKey="dbAccountInserted",
            #                         message=json.dumps(self, cls=AlchemyEncoder))
        except Exception as e:
            self.session.rollback()
            raise



from sqlalchemy.ext.declarative import DeclarativeMeta
class AlchemyEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj.__class__, DeclarativeMeta):
            # an SQLAlchemy class
            fields = {}
            for field in [x for x in dir(obj) if not x.startswith('_') and x != 'metadata']:
                data = obj.__getattribute__(field)
                try:
                    json.dumps(data) # this will fail on non-encodable values, like other classes
                    fields[field] = data
                except TypeError:
                    fields[field] = None
            # a json-encodable dict
            return fields

        return json.JSONEncoder.default(self, obj)