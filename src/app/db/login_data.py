from config import Config
from sqlalchemy import Column, DateTime, Integer, String, Text, Sequence, Boolean
from db.abstract import DbEntity
from sqlalchemy.ext.declarative import declarative_base
from pbglobal.pblib.amqp import client
import copy
import datetime
import json


Base = declarative_base()
config = Config()


class LoginData(DbEntity, Base):
    __tablename__ = 'login_data'
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8mb4', 'mysql_collate': 'utf8mb4_general_ci'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    login = Column(String(255))
    owner = Column(String(255))
    data = Column(Text())

    def __init__(self):
        super().__init__()
        try:
            Base.metadata.create_all(Config.db_engine)
        except Exception as e:
            self.session.rollback()
            raise

    def __repr__(self):
        return "<LoginData(id='%s', login='%s', owner='%s', )>\r\n%s" % (
            self.id, self.login, self.owner, self.data)

    def save(self):
        self.fire()

    def commit(self):
        raise Exception("For events, usage of commit and add functions are forbidden. Either use save() oder fire()")

    def add(self):
        raise Exception("For events, usage of commit and add functions are forbidden. Either use save() oder fire()")

    def rollback(self):
        raise Exception("For events, usage of rollback is forbidden.")

    def fire(self):
        message = copy.deepcopy(self.__dict__)
        if "_sa_instance_state" in message:
            del message["_sa_instance_state"]

        try:
            self.session.add(self)
            self.session.commit()
        except Exception as e:
            self.session.rollback()
            raise



from sqlalchemy.ext.declarative import DeclarativeMeta
class AlchemyEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj.__class__, DeclarativeMeta):
            # an SQLAlchemy class
            fields = {}
            for field in [x for x in dir(obj) if not x.startswith('_') and x != 'metadata']:
                data = obj.__getattribute__(field)
                try:
                    json.dumps(data) # this will fail on non-encodable values, like other classes
                    fields[field] = data
                except TypeError:
                    fields[field] = None
            # a json-encodable dict
            return fields

        return json.JSONEncoder.default(self, obj)