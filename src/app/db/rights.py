from config import Config
from sqlalchemy import Column, DateTime, Integer, String, Text, Sequence, Boolean
from db.abstract import DbEntity
from sqlalchemy.ext.declarative import declarative_base
from pbglobal.pblib.amqp import client
import copy
import datetime
import json


Base = declarative_base()
config = Config()


class Rights(DbEntity, Base):
    __tablename__ = 'rights'
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8mb4', 'mysql_collate': 'utf8mb4_general_ci'}

    owner = Column(String(255))
    creator = Column(String(255))
    name = Column(String(255), primary_key=True)
    description = Column(String(255))
    definition = Column(Text)
    creationTime = Column(DateTime,  default=datetime.datetime.utcnow)
    data_owners = Column(Text, default='["role.sys_admin"]')

    def __init__(self):
        super().__init__()
        try:
            Base.metadata.create_all(Config.db_engine)
        except Exception as e:
            self.session.rollback()
            raise

    def __repr__(self):
        return "<Right(owner='%s', creator='%s', name='%s', description='%s', data_owners='%s')>" % (
            self.owner, self.creator, self.name, self.description, self.data_owners)

    def insert_defaults(self):
        with open("/src/app/ressources/default_rights.json") as f:
            jsonObject = json.load(f)

            Config.db_session.query(Rights).filter(Rights.name == "defaults").delete()
            Config.db_session.commit()

            right = Rights()
            right.owner = "system"
            right.creator = "system"
            right.name = "defaults"
            right.description = ""
            right.definition = json.dumps(jsonObject)
            right.creationTime = datetime.datetime.now().isoformat()
            right.save()

        with open('/src/app/ressources/default_roles.json') as f:
            json_object = json.load(f)

            for key, value in json_object.items():
                Config.db_session.query(Rights).filter(Rights.name == key).delete()
                Config.db_session.commit()

                right = Rights()
                right.owner = "system"
                right.creator = "system"
                right.name = key
                right.description = ""
                right.definition = json.dumps(value)
                right.creationTime = datetime.datetime.now().isoformat()
                right.save()

    def save(self):
        self.fire()

    def commit(self):
        raise Exception("For events, usage of commit and add functions are forbidden. Either use save() or fire()")

    def add(self):
        raise Exception("For events, usage of commit and add functions are forbidden. Either use save() or fire()")

    def rollback(self):
        raise Exception("For events, usage of rollback is forbidden.")

    def fire(self):
        message = copy.deepcopy(self.__dict__)
        if "_sa_instance_state" in message:
            del message["_sa_instance_state"]

        try:
            self.session.add(self)
            self.session.commit()
        except Exception as e:
            self.session.rollback()
            raise



from sqlalchemy.ext.declarative import DeclarativeMeta
class AlchemyEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj.__class__, DeclarativeMeta):
            # an SQLAlchemy class
            fields = {}
            for field in [x for x in dir(obj) if not x.startswith('_') and x != 'metadata']:
                data = obj.__getattribute__(field)
                try:
                    json.dumps(data) # this will fail on non-encodable values, like other classes
                    fields[field] = data
                except TypeError:
                    fields[field] = None
            # a json-encodable dict
            return fields

        return json.JSONEncoder.default(self, obj)