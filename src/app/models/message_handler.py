import requests
import json
from time import sleep
import os
from pbglobal.pblib.amqp import client
from pbglobal.pblib.pbrsa import decrypt
from pblib.mail_send import send_registration_mail, send_welcome_mail
from db.account import Account
from db.logins import Logins
from db.login_rights import LoginRights
from db.login_data import LoginData
from config import Config
from pbglobal.pblib.kong import Kong
from pbglobal.events.accountCreated import accountCreated
from pbglobal.events.accountConfirmed import accountConfirmed
from pbglobal.commands.createLogin import createLogin
from pbglobal.events.loginCreated import loginCreated
from pbglobal.events.loginDeleted import loginDeleted
from pbglobal.events.accountDeleted import accountDeleted
from pbglobal.commands.deleteLogin import deleteLogin
from pbglobal.commands.addRightsRole import addRightsRole
from pbglobal.events.rightsRoleAdded import rightsRoleAdded
from pbglobal.events.rightsRoleDeleted import rightsRoleDeleted
from pbglobal.events.accountProfileDataUpdated import accountProfileDataUpdated
from pbglobal.events.loginProfileDataUpdated import loginProfileDataUpdated
from pbglobal.events.accountDataOwnerAddedToRole import accountDataOwnerAddedToRole
from pbglobal.events.accountDataOwnerRemovedFromRole import accountDataOwnerRemovedFromRole
from pbglobal.events.loginPasswordResetted import loginPasswordResetted
from pbglobal.pblib.helpers import get_consumer_id_by_custom_id
from db.rights import Rights
from models.rights import RightsModel
from datetime import datetime
import traceback
from uuid import uuid4
from sqlalchemy import or_
from ipaddress import ip_address


class message_handler():
    kong_api = "http://kong.planblick.svc:8001"
    queue_client = client()

    def handle_message(ch, method=None, properties=None, body=None):
        try:
            print("---------- Start handling Message ----------")
            event = method.routing_key
            body = body.decode("utf-8")
            instance = message_handler()
            if (event is not None):
                handler_exists = method.routing_key in dir(instance)

                if handler_exists is True:
                        if getattr(instance, method.routing_key)(ch, body) == True:
                            ch.basic_ack(method.delivery_tag)
                        else:
                            ch.basic_nack(method.delivery_tag)
                else:
                    raise Exception("No handler for this  message or even not responsible: '" + event + "'")

        except Exception as e:
            print(e)
            traceback.print_exc()
            ch.basic_nack(method.delivery_tag)
            sleep(15)

        print("---------- End handling Message ----------")

    def resetLoginPassword(self, ch, msg):
        print("Handling resetLoginPassword")
        command_payload = json.loads(msg)
        event = loginPasswordResetted.from_json(loginPasswordResetted, command_payload)
        event.publish()
        return True

    def loginPasswordResetted(self, ch, msg):
        print("Handling loginPasswordResetted")
        payload = json.loads(msg)
        consumer_id = get_consumer_id_by_custom_id(payload.get("consumer_id"))
        login = payload.get("username")
        password = payload.get("password")
        try:
            password = decrypt(password)
        except(Exception):
            password = password

        Kong().reset_password_of_login(consumer_id=consumer_id, login=login, password=password)

        message = {"event": "loginPasswordResetted",
                   "args": {"correlation_id": payload.get("correlation_id"),
                            "login": login}}

        payload = {"room": payload.get("consumer_id"), "data": message}
        ch.basic_publish(exchange="socketserver", routing_key="notifications", body=json.dumps(payload))

        return True

    def accountAddDataOwnerToRole(self, ch, msg):
        print("Handling updateLoginProfileData")
        command_payload = json.loads(msg)
        event = accountDataOwnerAddedToRole.from_json(accountDataOwnerAddedToRole, command_payload)
        event.publish()
        return True

    def accountDataOwnerAddedToRole(self, ch, msg):
        try:
            print("Handling accountDataOwnerAddedToRole")
            json_data = json.loads(msg)
            print(json_data)
            new_owners = json_data.get("data", {}).get("owners")

            right = Config.db_session.query(Rights).filter(Rights.name == json_data.get("data", {}).get("role")).one_or_none()

            if right is not None:
                current_owners = json.loads(right.data_owners)
                merged_owners = current_owners + [owner for owner in new_owners if owner not in current_owners]
                print("NEW OWNERS", current_owners, new_owners, merged_owners)
                right.data_owners = json.dumps(merged_owners)
                right.save()
        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            return False

        return True

    def accountRemoveDataOwnerFromRole(self, ch, msg):
        print("Handling updateLoginProfileData")
        command_payload = json.loads(msg)
        event = accountDataOwnerRemovedFromRole.from_json(accountDataOwnerRemovedFromRole, command_payload)
        event.publish()
        return True

    def accountDataOwnerRemovedFromRole(self, ch, msg):
        try:
            print("Handling accountDataOwnerAddedToRole")
            json_data = json.loads(msg)
            print(json_data)
            new_owners = json_data.get("data", {}).get("owners")

            right = Config.db_session.query(Rights).filter(
                Rights.name == json_data.get("data", {}).get("role")).one_or_none()

            if right is not None:
                current_owners = json.loads(right.data_owners)
                merged_owners = [owner for owner in current_owners if owner not in new_owners]
                print("NEW OWNERS", current_owners, new_owners, merged_owners)
                right.data_owners = json.dumps(merged_owners)
                right.save()
        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            return False

        return True

    def updateLoginProfileData(self, ch, msg):
        print("Handling updateLoginProfileData")
        command_payload = json.loads(msg)
        event = loginProfileDataUpdated.from_json(loginProfileDataUpdated, command_payload)
        event.publish()
        return True

    def loginProfileDataUpdated(self, ch, msg):
        print("Handling loginProfileDataUpdated")
        json_data = json.loads(msg)
        profile_updated_event = loginProfileDataUpdated.from_json(loginProfileDataUpdated, json_data)

        profile_data = Config.db_session.query(LoginData) \
            .filter(LoginData.login == profile_updated_event.login) \
            .filter((LoginData.owner == profile_updated_event.owner_id) | (LoginData.owner == "")) \
            .first()

        if not profile_data:
            profile_data = LoginData()


        profile_data.login = profile_updated_event.login
        profile_data.owner = profile_updated_event.owner_id
        profile_data.data = json.dumps(profile_updated_event.data)
        profile_data.save()

        message = {"command": "reloadLoginProfileData",
                   "args": {"login": profile_updated_event.login, "task_id": json_data.get("correlation_id")}}

        payload = {"room": profile_updated_event.login, "data": message}
        client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))

        return True

    def updateAccountProfileData(self, ch, msg):
        print("Handling updateAccountProfileData")
        command_payload = json.loads(msg)
        event = accountProfileDataUpdated.from_json(accountProfileDataUpdated, command_payload)
        event.publish()
        return True

    def accountProfileDataUpdated(self, ch, msg):
        json_data = json.loads(msg)
        profile_updated_event = accountProfileDataUpdated.from_json(accountProfileDataUpdated, json_data)
        print("Handling accountProfileDataUpdated", profile_updated_event.account_id)
        profile_data = Config.db_session.query(Account) \
            .filter(Account.owner == profile_updated_event.account_id) \
            .first()

        if profile_data is None:
            profile_data = Account()
            profile_data.account_id = profile_updated_event.owner
            profile_data.creator = profile_updated_event.owner
            profile_data.owner = profile_updated_event.owner
            profile_data.confirmed = 1
            profile_data.correlation_id = json_data.get("correlation_id")

        profile_data.profile_data = json.dumps(profile_updated_event.data)
        profile_data.save()

        return True

    def rightsRoleAdded(self, ch, msg):
        try:
            print("Handling rightsRoleAdded")
            json_data = json.loads(msg)
            print(json_data)
            rightsRole = addRightsRole.from_json(addRightsRole, json_data)

            rights = RightsModel(rightsRole.owner)
            creators_role_imports = rights.get_role_imports_by_user(owner=rightsRole.owner, user_id="user." + rightsRole.creator, ignore_owner=True)

            granted_roles = []
            with open("/src/app/ressources/default_roles.json") as f:
                jsonObject = json.load(f)

            is_caused_by_registration = False
            try:
                ip_address(json_data.get("creator"))
                is_caused_by_registration = True
            except ValueError:
                pass

            print("CREATORS-ROLE-IMPORTS", rightsRole.owner, rightsRole.creator, creators_role_imports)

            for role in rightsRole.definition.get("import", []):
                if ((rightsRole.creator != "superuser") and "role.sys_admin" not in creators_role_imports and "role.admin" not in creators_role_imports) and role not in creators_role_imports and (is_caused_by_registration is False or jsonObject.get(role, {}).get("available_for_registration", False) is False) :
                    message = {"command": "showTaskError",
                               "args": {"text": "You are not allowed to grant this role",
                                        "task_id": json_data.get("correlation_id")}}
                    payload = {"room": rightsRole.owner, "data": message}
                    client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
                else:
                    granted_roles.append(role)

            rightsRole.definition["import"] = granted_roles

            Config.db_session.query(Rights).filter(Rights.name == rightsRole.name).delete()
            Config.db_session.commit()

            right = Rights()
            right.owner = rightsRole.owner
            right.creator = rightsRole.creator
            right.name = rightsRole.name
            right.description = rightsRole.description
            right.definition = json.dumps(rightsRole.definition)
            right.creationTime = datetime.fromisoformat(rightsRole.creation_time).isoformat()
            right.data_owners = json.dumps(rightsRole.data_owners)
            right.save()

            RightsModel.users_rights.pop(rightsRole.owner + "_" + rightsRole.name, None)
            message = {"command": "refreshAccessRights",
                       "args": {}}
            print("MYCCHANNEL", rightsRole.owner)
            payload = {"room": rightsRole.owner, "data": message}
            client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            return False

        return True

    def addRightsRole(self, ch, msg):
        try:
            print("Handling addRightsRole")
            command_payload = json.loads(msg)
            event = rightsRoleAdded.from_json(rightsRoleAdded, command_payload)
            event.publish()

        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            return False

        return True

    def rightsRoleDeleted(self, ch, msg):
        try:
            print("Handling rightsRoleDeleted")
            json_data = json.loads(msg)
            Config.db_session.query(Rights).filter(Rights.name == json_data.get("name")).delete()
            Config.db_session.commit()

            message = {"event": "roleDeleted",
                       "args": json_data}
            payload = {"room": json_data.get("owner"), "data": message}
            client().publish(toExchange="socketserver", routingKey="notifications", message=json.dumps(payload))


        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            return False

        return True

    def deleteRightsRole(self, ch, msg):
        try:
            print("Handling deleteRightsRole")
            command_payload = json.loads(msg)
            event = rightsRoleDeleted.from_json(rightsRoleDeleted, command_payload)
            event.publish()
        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            return False

        return True

    def accountDeleted(self, ch, msg):
        try:
            print("TRYING TO DELETE ACCOUNT", msg)

            command_payload = json.loads(msg)
            event = accountDeleted.from_json(accountDeleted, command_payload)

            if event.owner != command_payload.get("id"):
                print("\n\n!!!ACCOUNT DELETION REJECTED, REQUESTER IS NOT OWNER!!!\n\n")
                message = {"event": "accountDeletionFailed",
                           "args": {"reason_id": 2, "reason": "requester not owner of account",
                                    "event_data": event.to_string()}}
                payload = {"room": event.owner, "data": message}
                self.queue_client.publish(toExchange="socketserver", routingKey="notifications",
                                          message=json.dumps(payload))
                return True

            # Check whether customer_id exists
            url = self.kong_api + f"/consumers/{command_payload.get('id')}"
            response = requests.request("GET", url)

            if response.status_code == 404:
                print("\n\n!!!ACCOUNT DELETION TRIED ON NON EXISTING ID: ", command_payload.get('id')), "\n\n"
                message = {"event": "accountDeletionFailed",
                           "args": {"reason_id": 1, "reason": "account does not exist",
                                    "event_data": json.loads(event.to_string())}}
                payload = {"room": event.owner, "data": message}
                self.queue_client.publish(toExchange="socketserver", routingKey="notifications",
                                          message=json.dumps(payload))
                return True

            # First cleanup login-table
            logins = Config.db_session.query(Account).filter(Account.account_id == command_payload.get("id"))
            logins.delete(synchronize_session=False)
            Config.db_session.commit()

            # Third: delete consumer
            url = self.kong_api + f"/consumers/{command_payload.get('id')}"
            response = requests.request("DELETE", url)
            print("RESPONSE STATUS CODE", str(response.status_code))
            if response.status_code == 204:
                return True
            else:
                return False

        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            return False

        return True

    def deleteAccount(self, ch, msg):
        command_payload = json.loads(msg)
        accountDeleted.from_json(accountDeleted, command_payload).publish()
        return True

    def loginDeleted(self, ch, msg):
        try:
            command_payload = json.loads(msg)
            event = loginDeleted.from_json(loginDeleted, command_payload)

            # First get consumer-id of credential username
            url = self.kong_api + f"/basic-auths/{command_payload.get('username')}"
            print("CALLING DELETE URL:", url)
            payload = {"username": command_payload.get('username')}
            headers = {'Content-Type': "application/json"}
            response = requests.request("GET", url, data=json.dumps(payload), headers=headers)
            username = response.json().get("username", "")
            print("RESPONSE", response.json())

            print("DELETING LOGIN DATA FOR", command_payload.get("username"), username)

            # Third cleanup login-table
            logins = Config.db_session.query(Logins).filter(or_(Logins.login_name == command_payload.get("username"), Logins.login_name == username))
            logins.delete(synchronize_session="fetch")
            logins = Config.db_session.query(LoginData).filter(or_(LoginData.login == command_payload.get("username"), LoginData.login == username))
            logins.delete(synchronize_session="fetch")
            logins = Config.db_session.query(LoginRights).filter(or_(LoginRights.login_name == command_payload.get("username"), LoginRights.login_name == username))
            logins.delete(synchronize_session="fetch")
            logins = Config.db_session.query(Rights).filter(Rights.name == f"user.{username}")
            logins.delete(synchronize_session="fetch")
            Config.db_session.commit()

            if response.status_code == 404:
                print("\n\n!!!LOGIN DELETION TRIED ON NON EXISTING LOGIN: ", command_payload.get('username')), "\n\n"
                message = {"event": "loginDeletionFailed",
                           "args": {"reason_id": 1, "reason": "login does not exist",
                                    "event_data": json.loads(event.to_string())}}
                payload = {"room": event.owner, "data": message}
                self.queue_client.publish(toExchange="socketserver", routingKey="notifications",
                                          message=json.dumps(payload))
                return True

            consumer_id = response.json().get("consumer").get("id")

            # Test whether deletion request comes from account within same consumer
            print("OWNER", event.owner)
            gateway_consumer_id = get_consumer_id_by_custom_id(event.owner)
            print("ID", consumer_id)
            if gateway_consumer_id != consumer_id:
                print("\n\n!!!LOGIN DELETION REJECTED, REQUESTER IS NOT USER OF OWNER!!!\n\n")
                message = {"event": "loginDeletionFailed", "args": {"reason": "requester is not member of same account",
                                                                    "event_data": json.loads(event.to_string())}}
                payload = {"room": event.owner, "data": message}
                self.queue_client.publish(toExchange="socketserver", routingKey="notifications",
                                          message=json.dumps(payload))
                return True

            # Second: delete credential
            url = self.kong_api + f"/consumers/{consumer_id}/basic-auth/{command_payload.get('username')}"
            requests.request("DELETE", url)

            message = {"event": "loginDeleted", "args": json.loads(event.to_string())}
            payload = {"room": event.owner, "data": message}
            self.queue_client.publish(toExchange="socketserver", routingKey="notifications",
                                      message=json.dumps(payload))

            #


        except Exception as e:
            print(e)
            Config.db_session.rollback()
            return False

        return True

    def deleteLogin(self, ch, msg):
        command_payload = json.loads(msg)
        deleteLogin.from_json(loginDeleted, command_payload).publish()
        return True

    def createAccount(self, ch, msg):
        command_payload = json.loads(msg)
        Config.db_session.expire_all()
        newaccount = Config.db_session.query(Account).filter_by(login=command_payload.get("username")).first()

        if newaccount is not None and newaccount.confirmed != 0:
            # Wait a short moment to make sure client is already listening
            sleep(0.05)
            message = {"event": "accountCreationFailed",
                       "args": {"correlation_id": command_payload.get("correlation_id"),
                                "error_message": "Login already exists"}}
            payload = {"room": "tmp_global", "data": message}
            self.queue_client.publish(toExchange="socketserver", routingKey="notifications",
                                      message=json.dumps(payload))
            return True

        command_payload = json.loads(msg)
        event = accountCreated.from_json(accountCreated, command_payload)
        event.publish()

        # Send registration email now
        send_registration_mail(recipient=command_payload.get("email"),
                               registration_key=command_payload.get("correlation_id"))

        # Wait a short moment to make sure client is already listening
        sleep(0.05)
        message = {"event": "confirmationMailSent", "args": {"correlation_id": command_payload.get("correlation_id")}}
        payload = {"room": "tmp_global", "data": message}
        self.queue_client.publish(toExchange="socketserver", routingKey="notifications",
                                  message=json.dumps(payload))

        return True

    def accountCreated(self, ch, msg):
        try:
            command_payload = json.loads(msg)
            newaccount = Config.db_session.query(Account).filter_by(login=command_payload.get("username")).first()

            print("NEWACOUNT  IST ", newaccount)

            if not newaccount:
                newaccount = Account()
            newaccount.account_id = ""
            newaccount.email = command_payload.get("email")
            newaccount.login = command_payload.get("username")
            newaccount.creator = ""
            newaccount.owner = ""
            newaccount.password = command_payload.get("password")
            newaccount.data = msg
            newaccount.correlation_id = command_payload.get("correlation_id")
            newaccount.version = 1

            newaccount.save()

            # Wait a short moment to make sure client is already listening
            sleep(0.05)
            message = {"event": "accountCreated", "args": {"correlation_id": command_payload.get("correlation_id")}}
            payload = {"room": "tmp_global", "data": message}
            self.queue_client.publish(toExchange="socketserver", routingKey="notifications",
                                      message=json.dumps(payload))

            return True
        except Exception as e:
            print(e)
            Config.db_session.rollback()
            return False

    def confirmAccount(self, ch, msg):
        print("IN confirmAccount")
        registration_key = "foobar"

        command_payload = json.loads(msg)
        print("Confirm account payload", command_payload)

        try:
            newaccount = Config.db_session.query(Account).filter_by(
                correlation_id=command_payload.get("registration_key")).first()
            Config.db_session.commit()
            if newaccount is not None:
                # Send registration email now
                send_welcome_mail(recipient=newaccount.email)

                account_confirmed_payload = command_payload
                account_confirmed_payload["correlation_id"] = command_payload.get("correlation_id")

                event = accountConfirmed.from_json(accountConfirmed, account_confirmed_payload)
                event.publish()
                # self.queue_client.publish("message_handler", "accountConfirmed", json.dumps(account_confirmed_payload))

                message = {"event": "accountConfirmed", "args": event.to_string()}
                payload = {"room": "log", "data": message}
                self.queue_client.publish(toExchange="socketserver", routingKey="log", message=json.dumps(payload))
        except Exception as e:
            Config.db_session.rollback()
            print(e)
            import traceback
            print(traceback.format_exc())
            return False

        return True

    def accountConfirmed(self, ch, msg):
        print("Handling accountConfirmed Command", msg)

        command_payload = json.loads(msg)
        newaccount = Config.db_session.query(Account).filter_by(
            correlation_id=command_payload.get("registration_key")).first()
        Config.db_session.commit()
        if newaccount is None:
            return True

        newaccount.confirmed = True
        newaccount.save()



        # Delete double entries with this email which were not confirmed to make sure we have only one record for this email
        try:
            Config.db_session.query(Account).filter_by(email=newaccount.email, confirmed=0).delete()
            Config.db_session.commit()
        except Exception as e:
            print(e)
            Config.db_session.rollback()
            pass

        url = self.kong_api + "/consumers/"
        print("Calling: ", url)
        new_custom_id = newaccount.correlation_id
        payload = {"username": newaccount.login, "custom_id": new_custom_id}
        headers = {'Content-Type': "application/json"}

        response = requests.request("POST", url, data=json.dumps(payload), headers=headers)

        print("Result of kong create account:", response.status_code, response.text)
        # If we got a duplicate error from kong, retrieve consumer_id of existing one
        if response.status_code == 409:
            response = requests.request("GET", self.kong_api + f"/consumers/{payload.get('username')}",
                                        data=json.dumps(payload), headers=headers)

        new_customer_id = response.json().get("id")
        print("NEW CUSTOMER KONG-ID:", new_customer_id)
        new_account_data = json.loads(newaccount.data)
        if (response.status_code == 201 or response.status_code == 200 or response.status_code == 409):
            create_login_payload = {"correlation_id": command_payload.get("correlation_id"),
                                    "consumer_id": new_custom_id,
                                    "username": newaccount.login,
                                    "password": newaccount.password,
                                    "owner": new_custom_id,
                                    "creator": command_payload.get("creator"),
                                    "roles": new_account_data.get("roles", []),
                                    "email": newaccount.email
                                    }

            event = createLogin.from_json(createLogin, create_login_payload)
            event.publish()

            newaccount.owner = new_custom_id
            newaccount.creator = new_custom_id
            newaccount.account_id = new_custom_id
            newaccount.save()

            profileData = {"firstname": "", "lastname": "", "email": newaccount.email}
            event = loginProfileDataUpdated.from_json(loginProfileDataUpdated, command_payload, True)
            # event = loginProfileDataUpdated(consumer_id=newaccount.account_id, login=newaccount.login, data=profileData, correlation_id=newaccount.correlation_id, owner=newaccount.account_id)
            event.owner_id = newaccount.account_id
            event.owner = newaccount.account_id
            event.login = newaccount.login
            event.data = profileData
            event.create_time = command_payload.get("create_time")
            event.validate()
            event.publish()

            result = Config.db_session.query(Rights).filter(Rights.name == f"user.{newaccount.login}").one_or_none()
            Config.db_session.commit()

            message = {"event": "kongAccountCreated", "args": json.dumps(command_payload)}
            payload = {"room": "log", "data": message}
            self.queue_client.publish(toExchange="socketserver", routingKey="log", message=json.dumps(payload))

        groups = [
            newaccount.email,
            str(newaccount.email) + "_admin",
            str(newaccount.email) + "_user",
            "quicksign",
            "user",
            "easy2track"
        ]

        for group in groups:
            payload = {"group": group}
            headers = {
                'Content-Type': "application/json"
            }
            url = self.kong_api + "/consumers/" + new_customer_id + "/acls"
            response = requests.request("POST", url, data=json.dumps(payload), headers=headers)
            print(response.status_code, response.text)
            if response.status_code != 201 and response.status_code != 200 and response.status_code != 409 and (
                    response.status_code != 400 and 'ACL group already exist for this consumer' in response.text):
                return False

        return True

    def createLogin(self, ch, msg):
        try:
            print("IN createLogin")
            msg = json.loads(msg)
            print("PAYLOAD:", msg)
            Config.db_session.expire_all()
            result = Config.db_session.query(LoginData).filter(LoginData.login == msg.get("username")).first()
            Config.db_session.commit()

            if result is not None:
                #Wait a short moment to make sure client is already listening
                sleep(0.05)
                message = {"event": "loginCreationFailed", "args": {"correlation_id": msg.get("correlation_id"), "error_message": "Login already exists"}}
                payload = {"room": "tmp_global", "data": message}
                self.queue_client.publish(toExchange="socketserver", routingKey="notifications",
                                          message=json.dumps(payload))
                return True

            login_created_payload = {"correlation_id": msg.get("correlation_id"),
                                     "consumer_id": msg.get("consumer_id"),
                                     "owner": msg.get("owner", msg.get("consumer_id")),
                                     "creator": msg.get("owner", msg.get("consumer_id")),
                                     "username": msg.get("username"),
                                     "password": msg.get("password"),
                                     "email": msg.get("email")
                                     }
            print("PUSH LOGIN CREATED MESSAGE")
            # self.queue_client.publish("message_handler", "loginCreated", json.dumps(login_created_payload))
            event = loginCreated.from_json(loginCreated, login_created_payload)
            event.publish()

            if msg.get("email"):
                profileData = {"firstname": "", "lastname": "", "email": msg.get("email")}
                event = loginProfileDataUpdated.from_json(loginProfileDataUpdated, login_created_payload, True)
                event.owner_id = msg.get("consumer_id")
                event.owner = msg.get("consumer_id")
                event.login = msg.get("username")
                event.creator = msg.get("creator")
                event.correlation_id = msg.get("correlation_id")
                event.data = profileData
                event.create_time = msg.get("create_time")
                event.validate()
                event.publish()

            if msg.get("roles"):
                json_data = {
                    "definition": {"import": msg.get("roles"), "rules": {}},
                    "description": "",
                    "name": "user." + msg.get("username", ""),
                    "owner": msg.get("owner"),
                    "correlation_id": msg.get("correlation_id"),
                    "creator": msg.get("creator"),
                    "create_time": msg.get("create_time")
                }
                addRightsRole.from_json(addRightsRole, json_data).publish()

            return True
        except Exception as e:
            print(e)
            return False

    def loginCreated(self, ch, msg):
        try:
            print("IN LOGIN CREATED")
            msg = json.loads(msg)
            consumer_id = msg.get("consumer_id")
            print(msg)
            gateway_consumer_id = get_consumer_id_by_custom_id(consumer_id)
            url = self.kong_api + "/consumers/" + gateway_consumer_id + "/basic-auth"
            print("Calling: ", url)
            try:
                password = decrypt(msg.get("password"))
            except(Exception):
                password = str(uuid4())

            payload = {"username": msg.get("username"), "password": password}
            headers = {'Content-Type': "application/json"}

            response = requests.request("POST", url, data=json.dumps(payload), headers=headers)
            print(response.status_code)
            if response.status_code == 201 or response.status_code == 200 or response.status_code == 409:
                login_created_payload = {"correlation_id": msg.get("correlation_id"),
                                         "consumer_id": msg.get("consumer_id"),
                                         "username": msg.get("username"),
                                         "password": msg.get("password")}

                default_login_rights = {
                    "rights": {
                        "multi_session_allowed": False,
                        "account_admin": False
                    },
                    "available_apps": ["quicksign", "easy2track", "easy2schedule"]
                }
                login_rights = Config.db_session.query(LoginRights.data).filter(
                    LoginRights.login_name == msg.get("username")).one_or_none()
                if login_rights is None:
                    login_right = LoginRights()
                    login_right.login_name = msg.get("username")
                    login_right.owner = msg.get("consumer_id")
                    login_right.data = json.dumps(default_login_rights)
                    login_right.save()

                message = {"event": "loginCreated", "args": msg}
                payload = {"room": msg.get("consumer_id"), "data": message}
                self.queue_client.publish(toExchange="socketserver", routingKey="notifications",
                                          message=json.dumps(payload))
                return True
            else:
                message = {"event": "loginCreationFailed", "args": msg, "response": response}
                payload = {"room": msg.get("consumer_id"), "data": message}
                self.queue_client.publish(toExchange="socketserver", routingKey="notifications",
                                          message=payload)

                message = {"event": "loginCreationFailed", "args": {"correlation_id": msg.get("correlation_id")}}
                payload = {"room": "tmp_global", "data": message}
                self.queue_client.publish(toExchange="socketserver", routingKey="notifications",
                                          message=json.dumps(payload))
                return True

        except Exception as e:
            print(e)
            traceback.print_exc()
            return False
