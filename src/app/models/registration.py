from pbglobal.pblib.amqp import client

class Registration():
    def __init__(self, data: dict = None):
        self.data = data

    def register_account(self, data:dict = None):
        if data is None:
            data = self.data
        if data is None:
            return False

        queue = client()
        queue.publish("message_handler", "createAccount", self.data)
        return True

    def confirm_account(self, data:dict = None):
        if data is None:
            data = self.data
        if data is None:
            return False

        queue = client()
        queue.publish("message_handler", "confirmAccount", self.data)
        return True