openssl pkcs12 -in yourcert.pfx -nocerts -out yourcert.pem
openssl pkcs12 -in yourcert.pfx -clcerts -nokeys -out yourcert.crt
openssl rsa -in yourcert.pem -out yourcert.key