Sehr geehrte Nutzerin, sehr geehrter Nutzer,

wir freuen uns, dass sie sich für das Angebot von serviceBLICK der planBLICK GmbH entschieden haben.
Profitieren sie von nun an von all unseren Anwendungen, wo und wann sie wollen.

Für Anpassungen und Individualisierungen wenden sie sich bitte an info@planblick.com. Wir kontaktieren sie dann schnellstmöglich, um das weitere Vorgehen zur Realisierung ihrer Vorgaben zu besprechen.

Sie können sich über ihren Zugang zum Dashboard unter https://www.serviceblick.com/dashboard über ihre gebuchten Produkte informieren, diese verwalten sowie weitere Angebote dazu buchen.

Wir werden unsere Angebote ständig erweitern, sie haben dann jederzeit die Möglichkeit ein Upgrade durchzuführen. Über neue Angebote werden wir sie über Ihren Zugang informieren.

Sie haben noch Fragen - dann zögern sie bitte nicht uns einfach eine Email an info@planblick.com zu senden.


Freundliche Grüße,
Ihr Team der planBLICK GmbH



-------------------------------------------------------------------------------------------------------------------------------
planBLICK GmbH - Drachenseestr. 10 - 82373 München - Sitz der Gesellschaft: Münchnchen - Eingetragen beim Amtsgericht  Amtsgericht München HRB 215958 - USt.-ID-Nr.: DE299088982 GF.: Martin Wallmeier  - https://www.planblick.com

Pflichtinformationen gemäß Artikel 13 DSGVO
Im Falle des Erstkontakts sind wir gemäß Art. 12, 13 DSGVO verpflichtet, Ihnen folgende datenschutzrechtliche Pflichtinformationen zur Verfügung zu stellen:
Wenn Sie uns per E-Mail kontaktieren, verarbeiten wir Ihre personenbezogenen Daten nur, soweit an der Verarbeitung ein berechtigtes Interesse besteht (Art. 6 Abs. 1 lit. f DSGVO), Sie in die Datenverarbeitung eingewilligt haben (Art. 6 Abs. 1 lit. a DSGVO), die Verarbeitung für die Anbahnung, Begründung, inhaltliche Ausgestaltung oder Änderung eines Rechtsverhältnisses zwischen Ihnen und uns erforderlich sind (Art. 6 Abs. 1 lit. b DSGVO) oder eine sonstige Rechtsnorm die Verarbeitung gestattet.
Ihre personenbezogenen Daten verbleiben bei uns, bis Sie uns zur Löschung auffordern, Ihre Einwilligung zur Speicherung widerrufen oder der Zweck für die Datenspeicherung entfällt (z. B. nach abgeschlossener Bearbeitung Ihres Anliegens).
Zwingende gesetzliche Bestimmungen – insbesondere steuer- und handelsrechtliche Aufbewahrungsfristen – bleiben unberührt. Sie haben jederzeit das Recht, unentgeltlich Auskunft über Herkunft, Empfänger und Zweck Ihrer gespeicherten personenbezogenen Daten zu erhalten. Ihnen steht außerdem ein Recht auf Widerspruch, auf Datenübertragbarkeit und ein Beschwerderecht bei der zuständigen Aufsichtsbehörde zu. Ferner können Sie die Berichtigung, die Löschung und unter bestimmten Umständen die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten verlangen
 Details entnehmen Sie unserer Datenschutzerklärung (https://datenschutz.planblick.com).
