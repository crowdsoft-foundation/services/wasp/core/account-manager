from flask import Flask, jsonify, request, redirect, url_for
from time import strftime
import json
import requests
from flask_cors import CORS
import datetime
import os
from planblick.logger import Logger
from pbglobal.pblib.amqp import client
from pbglobal.pblib.pbrsa import encrypt
from db.logins import Logins
from db.login_rights import LoginRights
from db.account import Account
from db.login_data import LoginData
from models.registration import Registration
from config import Config
from pbglobal.commands.createAccount import createAccount
from pbglobal.commands.createLogin import createLogin
from pbglobal.commands.confirmAccount import confirmAccount
from pblib.mail_send import send_password_lost_mail, send_login_invitation_mail, send_contact_mail
from models.rights import RightsModel
from pbglobal.pblib.helpers import get_consumer_id_by_custom_id
from pbglobal.pblib.kong import Kong
import requests
import threading
import random

app = Flask(__name__)
CORS(app)

# logserver_host = json.loads(os.getenv("CLUSTER_CONFIG")).get("logserver")
# log = Logger(logserver_host, 9999, consoleLog=False).getLogger()
lock = threading.Lock()

@app.route('/get_account_profile', methods=["GET"])
def get_account_profile():
    account_id = request.args.get("account_id")
    try:
        account = Config.db_session.query(Account.profile_data).filter(
            Account.account_id == account_id).one()
    except Exception:
        return jsonify({"error": "account_id not found"})

    result = json.loads(account[0]) if account[0] is not None else {}
    return jsonify(result)

@app.route('/get_login_profile', methods=["GET"])
def get_login_profile():
    login = request.args.get("login")
    try:
        account = Config.db_session.query(LoginData.data).filter(
            LoginData.login == login).all()[0]
    except Exception:
        return jsonify({"error": "login data not found"})

    return jsonify(json.loads(account[0]))

@app.route('/contact_mail', methods=["POST"])
def contact_mail():
    payload = request.json

    consumer_id = request.headers.get("X-Consumer-Custom-Id", "")
    print("CONSUMERID", consumer_id, request.headers)
    if consumer_id is None:
        return jsonify({"result": "consumer not found"}), 404

    result = send_contact_mail(payload=payload)

    return jsonify({"result": result})


@app.route('/login_invite', methods=["POST"])
def login_invite():
    payload = request.json

    consumer_id = request.headers.get("X-Consumer-Custom-Id", "")
    print("CONSUMERID", consumer_id, request.headers)
    if consumer_id is None:
        return jsonify({"result": "consumer not found"}), 404

    key = create_new_key(consumer_id).get("key", None)
    if key is None:
        return jsonify({"result": "Sending invitation not possible, please contact support",
                        "error_code": "key not obtainable"}), 400

    result = send_login_invitation_mail(recipient=payload.get("username"), temporary_api_key=key)

    return jsonify({"result": result})


@app.route('/init_credentials_lost_process', methods=["POST"])
def init_credentials_lost_process():
    payload = request.json
    consumer_id = get_consumer_id_by_basic_auth(payload.get("username"))

    loginData = Config.db_session.query(LoginData).filter(
    LoginData.login == payload.get("username")).first()


    print("LOGINDATA", loginData.login)
    if consumer_id is None:
        return jsonify({"result": True}), 200

    key = create_new_key(consumer_id).get("key", None)
    if key is None:
        return jsonify({"result": "Resetting password not possible, please contact support",
                        "error_code": "key not obtainable"}), 200

    result = send_password_lost_mail(recipient=json.loads(loginData.data).get("email"), temporary_api_key=key, user_login=loginData.login)

    return jsonify({"result": True})


@app.route('/update_credentials', methods=["POST"])
def update_password():
    request_payload = request.json
    consumer_id = request.headers.get("X-Consumer-Username", "")
    apikey = request.headers.get("apikey", "")
    return Kong(apikey=apikey).reset_password_of_login(consumer_id=consumer_id, login=request_payload.get("username"), password=request_payload.get("password"))

@app.route('/registration', methods=["POST"])
def publish_registration_event():
    try:
        print(request.headers)
        payload = request.json
        payload["password"] = encrypt(payload["password"])
        correlation_id = request.headers.get("X-Correlation-ID", "noCorrelationIdSet")
        payload["correlation_id"] = correlation_id
        payload["owner"] = request.headers.get("X-Real-Ip", "")
        payload["creator"] = request.headers.get("X-Real-Ip", "")

        result = createAccount.from_json(createAccount, payload).publish()
        
        if not result:
            return json.dumps({"error": "Cannot perform registration"}), 400
        # if not Registration(payload).register_account():
        #     return json.dumps({"error": "Cannot perform registration"}), 400

        return json.dumps({"taskid": correlation_id})
    except AssertionError as e:
        return jsonify({"error": str(e)}), 400


@app.route('/newlogin', methods=["POST"])
def create_login():
    try:
        payload = request.json
        payload["username"] = payload["username"]
        payload["password"] = encrypt(payload["password"])
        correlation_id = request.headers.get("X-Correlation-ID", "noCorrelationIdSet")
        payload["correlation_id"] = correlation_id
        payload["owner"] = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
        payload["creator"] = request.headers.get("X-Real-Ip", "")
        payload["consumer_id"] = request.headers.get("X-Consumer-Custom-Id", "")
        payload["create_time"] = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
        payload["roles"] = payload.get("roles", [])

        apikey = request.headers.get("apikey", "")

        consumer_id = request.headers.get("X-Consumer-Custom-Id", "")
        users_raw = get_basic_auth_by_consumer_id(consumer_id)

        try:
            login = None
            data = Config.db_session.query(Logins.login_name).filter(Logins.apikey == apikey).order_by(
                Logins.creationTime.desc()).first()
            Config.db_session.commit()
        except Exception:
            Config.db_session.rollback()
            login = None
            data = Config.db_session.query(Logins.login_name).filter(Logins.apikey == apikey).order_by(
                Logins.creationTime.desc()).first()
            Config.db_session.commit()

        if data is not None:
            (login,) = data
        else:
            login = data

        if login is not None:
            payload["creator"] = login


        result = createLogin.from_json(createLogin, payload).publish()
        
        if not result:
            return json.dumps({"error": "Cannot create login"}), 400
        # if not Registration(payload).register_account():
        #     return json.dumps({"error": "Cannot perform registration"}), 400

        return jsonify({"taskid": correlation_id})
    except AssertionError as e:
        return jsonify({"error": str(e)}), 400


@app.route('/confirm_registration', methods=["GET"])
def confirm_registration():
    registration_key = request.args.get("k")
    correlation_id = request.headers.get("X-Correlation-ID", "noCorrelationIdSet")
    payload = {"registration_key": registration_key, "correlation_id": correlation_id}
    payload["owner"] = request.headers.get("X-Real-Ip", "")
    payload["creator"] = request.headers.get("X-Real-Ip", "")

    result = confirmAccount.from_json(confirmAccount, payload).publish()
    
    if not result:
        return json.dumps({"error": "Cannot perform registration"}), 400

    return jsonify({"taskid": correlation_id, "payload": payload})
    # if not Registration(payload).confirm_account():
    #     return json.dumps({"error": "Cannot perform confirmation"}), 400
    #
    # return jsonify({"taskid": correlation_id, "payload": payload})


@app.route('/get_users', methods=["GET"])
def get_users_action():
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "")

    users_raw = get_basic_auth_by_consumer_id(consumer_id)
    users = []

    for user in users_raw:
        data = {"username": user.get("username"), "userid": user.get("id")}
        users.append(data)
    return jsonify(users), 200


@app.route('/get_login_by_apikey', methods=["GET"])
def get_login_by_apikey_action():
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "")
    #gateway_consumer_id = get_consumer_id_by_custom_id(consumer_id)
    users_raw = get_basic_auth_by_consumer_id(consumer_id)

    try:
        login = None
        apikey = request.args.get("apikey", "")
        data = Config.db_session.query(Logins.login_name).filter(Logins.apikey == apikey).order_by(
            Logins.creationTime.desc()).first()
        Config.db_session.commit()
    except Exception:
        Config.db_session.rollback()
        login = None
        apikey = request.args.get("apikey", "")
        data = Config.db_session.query(Logins.login_name).filter(Logins.apikey == apikey).order_by(
            Logins.creationTime.desc()).first()
        Config.db_session.commit()

    if data is not None:
        (login,) = data
    else:
        login = data

    user_id = None
    for user in users_raw:
        if login == user.get("username"):
            user_id = user.get("id")

    return jsonify({"login": login, "id": user_id, "consumer_id": consumer_id}), 200


@app.route('/login', methods=["GET"])
def login_action():
    account_id = request.headers.get("X-Consumer-Custom-Id", "")
    gateway_consumer_id = get_consumer_id_by_custom_id(account_id)
    username = request.headers.get("X-Credential-Username", "")
    consumername = request.headers.get("X-Consumer-Username", "")
    create_rabbitmq_vhost_for_consumer(consumername, username)
    clean_up_old_keys()

    login_rights = None
    try:
        login_rights = Config.db_session.query(LoginRights.data).filter(
            LoginRights.login_name == username).one_or_none()
    except Exception:
        pass

    if login_rights is not None:
        print("LOGIN RIGHTS", type(json.loads(login_rights[0])))
        multi_session_allowed = json.loads(login_rights[0]).get("rights", {}).get("multi_session_allowed", False)
    else:
        multi_session_allowed = False

    if not multi_session_allowed:
        for record in Config.db_session.query(Logins).filter(Logins.login_name == username):
            print("RECORD:", record)
            url = os.environ.get(
                "APIGATEWAY_ADMIN_INTERNAL_BASEURL") + f"/consumers/{gateway_consumer_id}/key-auth/{record.apikey}"
            payload = ""
            headers = {}
            response = requests.request("DELETE", url, headers=headers, data=payload)
            print("DELETE STATUS OF KONG:", response.status_code)
            if response.status_code == 204 or response.status_code == 404:
                Config.db_session.delete(record)
                Config.db_session.commit()

    url = os.environ.get("APIGATEWAY_ADMIN_INTERNAL_BASEURL") + "/consumers/" + gateway_consumer_id + "/key-auths"
    print(url)
    querystring = {}

    headers = {
        'Cache-Control': "no-cache"
    }

    # response = requests.request("GET", url, headers=headers, params=querystring)
    # kong_data = response.json()

    # first_entry = next(iter(kong_data.get("data", [])), None)
    # if first_entry is not None:
    #    data = first_entry
    # else:

    data = create_new_key(account_id)
    if "key" not in data:
        return jsonify({"message": "Not authenticated"}), 403

    data["apikey"] = data["key"]
    data["consumer_name"] = consumername
    data["username"] = username

    newlogin = Logins()
    newlogin.apikey = data["apikey"]
    newlogin.login_name = data["username"]
    newlogin.owner = account_id
    newlogin.correlation_id = request.headers.get("X-Correlation-ID", "noCorrelationIdSet")
    newlogin.fire()

    del (data["key"])
    return jsonify(data), 200


@app.route('/logout', methods=["GET"])
def logout_action():
    account_id = request.headers.get("X-Consumer-Custom-Id", "")
    apikey = request.headers.get("apikey", "")
    print("Logout for: ", account_id, apikey)
    clean_up_old_keys(consumer_id=account_id, apikey=apikey)

    return jsonify({"message": "OK"}), 200


@app.route('/checklogin', methods=["GET"])
def check_whether_login_exists():
    import requests
    gateway_url = json.loads(os.environ.get("CLUSTER_CONFIG")).get("kong-admin-url")
    login = request.args.get("login")
    url = gateway_url + "/basic-auths/" + login + "/consumer"

    response = requests.request("GET", url)

    if response.status_code != 200:
        return jsonify({"exists": False}), 200
    else:
        return jsonify({"exists": True}), 200


@app.route('/access/<groupname>')
@app.route('/access/<groupname>/<rightname>')
def has_group(groupname, rightname=None):
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "")
    result = False
    apikey = request.headers.get("apikey", "")

    (login,) = Config.db_session.query(Logins.login_name).filter(Logins.apikey == apikey).order_by(
        Logins.creationTime.desc()).first()
    Config.db_session.commit()
    print("LOGIN DATA:", login)
    if login is None:
        result = False
    else:
        with lock:
            data = Config.db_session.query(LoginRights).filter_by(login_name=login).first()
            Config.db_session.commit()
            print("RIGHTS DATA:", data)
            if data is None:
                result = False
                # Add default entry to login_name table to make system downward-compatible
                # CAn be removed when all accounts have an entry in accountmanager.login_rights
                default_login_rights = {
                    "rights": {
                        "multi_session_allowed": False,
                        "account_admin": False
                    },
                    "available_apps": ["quicksign", "easy2track", "easy2schedule"]
                }

                login_right = LoginRights()
                login_right.login_name = login
                login_right.owner = consumer_id
                login_right.data = json.dumps(default_login_rights)
                login_right.save()
                if groupname in default_login_rights.get("available_apps"):
                    result = True
                    result_code = 200
                    if rightname is not None:
                        if not default_login_rights.get("rights", {}).get(rightname, False):
                            result = False
                            result_code = 403
            else:
                print("DATA", json.loads(data.data).get("available_apps"), json.loads(data.data).get("rights"))
                if groupname in json.loads(data.data).get("available_apps"):
                    result = True
                    result_code = 200
                    if rightname is not None:
                        if not json.loads(data.data).get("rights", {}).get(rightname, False):
                            result = False
                            result_code = 403

    if result is False:
        result_code = 403
        if request.headers.get("X-Consumer-Groups") is not None and groupname in request.headers.get(
                "X-Consumer-Groups"):
            result = True
            result_code = 200

    return jsonify({"has_access": result}), result_code


@app.route('/get_roles', methods=["GET"])
def get_roles_list_action(rolename=None):
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "")
    apikey = request.headers.get("apikey", "")

    login, = Config.db_session.query(Logins.login_name).filter(Logins.apikey == apikey).order_by(
        Logins.creationTime.desc()).first()
    Config.db_session.commit()

    rights = RightsModel(consumer_id)
    user_rights = rights.get_rights_by_user_id(user_id="user." + login).get("rules")
    result = True if user_rights.get("dashboard.management.rolemanagement") and "allow" in user_rights.get(
        "dashboard.management.rolemanagement").get("action") else False
    if not result:
        return jsonify({"error": "not allowed", "user_rights": user_rights, "needed_rights": ["dashboard.management.rolemanagement"]}), 403

    rights = RightsModel(owner=consumer_id)
    roles = rights.get_roles(owner=consumer_id)
    return jsonify({"roles": roles}), 200


@app.route('/get_roles/bylogin/<login>', methods=["GET"])
def get_roles_bylogin_action(login=None):
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "")

    rights = RightsModel(consumer_id)
    user_rights = rights.get_role_imports_by_user(owner=consumer_id, user_id="user." + login)

    return jsonify({"roles": user_rights}), 200


@app.route('/get_group_members/<groupname>', methods=["GET"])
def get_group_members(groupname=None):
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "")

    rights = RightsModel(consumer_id)
    members = rights.get_members_of_group("group." + groupname)

    return jsonify({"members": members}), 200

@app.route('/is_in_group/<groupname>', methods=["GET"])
def is_in_group(groupname=None):
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "")
    apikey = request.headers.get("apikey", "")

    login, = Config.db_session.query(Logins.login_name).filter(Logins.apikey == apikey).order_by(
        Logins.creationTime.desc()).first()
    Config.db_session.commit()

    rights = RightsModel(consumer_id)
    role_definition = rights.get_role_definition_by_name("user." + login)
    print(role_definition)
    if role_definition is None:
        return jsonify({"is_in_group": False}), 200

    config = role_definition
    in_group = False

    if "group." + groupname in config.get("import", []):
        in_group = True

    return jsonify({"is_in_group": in_group}), 200

@app.route('/get_groups', methods=["GET"])
def get_groups_list_action(rolename=None):
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "")
    apikey = request.headers.get("apikey", "")

    login, = Config.db_session.query(Logins.login_name).filter(Logins.apikey == apikey).order_by(
        Logins.creationTime.desc()).first()
    Config.db_session.commit()

    rights = RightsModel(consumer_id)
    user_rights = rights.get_rights_by_user_id(user_id="user." + login).get("rules")
    result = True if user_rights.get("system.groups") and "allow" in user_rights.get(
        "system.groups").get("action") else False
    if not result:
        return jsonify({"error": "not allowed, missing access-rights", "user_rights": user_rights, "user_id":"user." + login, "needed_rights": ["system.groups"]}), 403

    rights = RightsModel(owner=consumer_id)
    groups = rights.get_groups(owner=consumer_id)
    return jsonify({"groups": groups}), 200

@app.route('/get_my_groups', methods=["GET"])
def get_my_groups_list_action(rolename=None):
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "")
    apikey = request.headers.get("apikey", "")

    login, = Config.db_session.query(Logins.login_name).filter(Logins.apikey == apikey).order_by(
        Logins.creationTime.desc()).first()
    Config.db_session.commit()

    rights = RightsModel(consumer_id)
    user_imports = rights.get_role_imports_by_user(owner=consumer_id, user_id="user." + login)


    return jsonify({"groups": user_imports}), 200


@app.route('/get_groups_by_login/<consumer_id>/<login>', methods=["GET"])
def get_groups_by_login_action(consumer_id=None, login=None):
    rights = RightsModel(consumer_id)
    user_imports = rights.get_role_imports_by_user(owner=consumer_id, user_id="user." + login)

    return jsonify({"groups": user_imports}), 200


@app.route('/get_rights/<user>', methods=["GET"])
@app.route('/get_rights', methods=["GET"])
def get_effective_rights_list_action(user=None):
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "")
    apikey = request.headers.get("apikey", "")
    login, = Config.db_session.query(Logins.login_name).filter(Logins.apikey == apikey).order_by(
        Logins.creationTime.desc()).first()
    Config.db_session.commit()
    rights = RightsModel(consumer_id)
    user_rights = rights.get_rights_by_user_id(user_id="user." + login).get("rules")
    result = True if user == "user." + login or user_rights.get(
        "dashboard") and "allow" in user_rights.get("dashboard").get(
        "action") else False
    if not result:
        return jsonify({"error": "not allowed", "needed_rights": ["dashboard"]}), 403

    rights = RightsModel(owner=consumer_id)
    user_rights = rights.get_rights_by_user_id(user_id=user)
    return jsonify(user_rights), 200


@app.route('/has_right/<right>', methods=["GET"])
def has_right_action(right):
    try:
        consumer_id = request.headers.get("X-Consumer-Custom-Id", "")
        apikey = request.headers.get("apikey", "")
        login, = Config.db_session.query(Logins.login_name).filter(Logins.apikey == apikey).order_by(
            Logins.creationTime.desc()).first()
        Config.db_session.commit()

        rights = RightsModel(consumer_id)
        user_rights = rights.get_rights_by_user_id(user_id="user." + login).get("rules")
        result = True if user_rights.get(right) and "allow" in user_rights.get(right).get("action") else False
    except Exception as e:
        return jsonify({"error": e.__repr__()})
    return jsonify({"result": result}), 200


@app.route('/has_valid_login')
def has_valid_login():
    return jsonify({"result": True}), 200


@app.route('/healthz', methods=["GET"])
def health_action():
    return jsonify({"message": "OK"}), 200


@app.route('/metrics', methods=["GET"])
def metrics_action():
    return jsonify({"message": "OK"}), 200


@app.route('/network', methods=["GET"])
def network_action():
    with open('/src/app/config.json') as f:
        data = json.load(f)
        subscribes = data.get("subscribes_to")
        emits = data.get("emits_events") + data.get("connects_to")

    assert isinstance(emits, (list))
    assert isinstance(subscribes, (list))

    return jsonify({"subscribes": subscribes, "emits": emits}), 200


def clean_up_old_guest_logins(max_age_in_minutes=10):
    url = os.environ.get("APIGATEWAY_ADMIN_INTERNAL_BASEURL") + "/basic-auths"

    headers = {
        'Cache-Control': "no-cache"
    }

    response = requests.request("GET", url, headers=headers)

    kong_data = response.json()

    for entry in kong_data.get("data", []):
        if not entry.get("username", "").startswith('guest_'):
            continue

        creation_time_utc = int(entry.get("created_at"))
        creation_datetime_utc = datetime.datetime.fromtimestamp(creation_time_utc)
        current_datetime_utc = datetime.datetime.utcnow()

        age = current_datetime_utc - creation_datetime_utc
        age_in_minutes = age.seconds / 60
        print("Guest-Login:", entry.get("username"), "Created", creation_datetime_utc, "Current", current_datetime_utc,
              "Age",
              age_in_minutes,
              "Max Age", max_age_in_minutes)
        if age_in_minutes >= max_age_in_minutes:
            print("Guest-Login-Timeout: Deleting Login " + entry.get("username") + " for " + entry.get("consumer", {}).get("id"))

            url = os.environ.get("APIGATEWAY_ADMIN_INTERNAL_BASEURL") + "/consumers/" + entry.get("consumer", {}).get(
                "id") + "/basic-auth/" + entry.get("id")

            headers = {
                'Cache-Control': "no-cache"
            }

            response = requests.request("DELETE", url, headers=headers)

def clean_up_old_keys(consumer_id=None, max_age_in_minutes=720, apikey=None):
    if consumer_id is not None and apikey is not None:
        url = os.environ.get("APIGATEWAY_ADMIN_INTERNAL_BASEURL") + f"/consumers/{consumer_id}/key-auth/{apikey}"
        payload = ""
        headers = {}
        response = requests.request("DELETE", url, headers=headers, data=payload)

    url = os.environ.get("APIGATEWAY_ADMIN_INTERNAL_BASEURL") + "/key-auths"

    headers = {
        'Cache-Control': "no-cache"
    }

    response = requests.request("GET", url, headers=headers)

    kong_data = response.json()

    for entry in kong_data.get("data", []):
        creation_time_utc = int((entry.get("created_at")) - 600)
        creation_datetime_utc = datetime.datetime.fromtimestamp(creation_time_utc)
        current_datetime_utc = datetime.datetime.utcnow()

        age = current_datetime_utc - creation_datetime_utc
        age_in_minutes = age.seconds / 60

        print("Key:", entry.get("id"), "Created", creation_datetime_utc, "Current", current_datetime_utc, "Age",
              age_in_minutes,
              "Max Age", max_age_in_minutes)

        # Hardcoded timeout for guest-keys
        if entry.get("consumer_id") == "7a61e63a-3d75-487c-9027-77e6f766d6bf" and age_in_minutes > 1440:
            url = os.environ.get("APIGATEWAY_ADMIN_INTERNAL_BASEURL") + "/consumers/" + entry.get(
                "consumer_id") + "/key-auth/" + entry.get("id")

            headers = {
                'Cache-Control': "no-cache"
            }

            response = requests.request("DELETE", url, headers=headers)
        elif age_in_minutes >= max_age_in_minutes:
            print("Key-timeout: Deleting apikey " + entry.get("key") + " for " + entry.get("consumer", {}).get("id"))

            url = os.environ.get("APIGATEWAY_ADMIN_INTERNAL_BASEURL") + "/consumers/" + entry.get("consumer", {}).get(
                "id") + "/key-auth/" + entry.get("id")

            headers = {
                'Cache-Control': "no-cache"
            }

            response = requests.request("DELETE", url, headers=headers)


def create_new_key(consumer_id):
    gateway_consumer_id = get_consumer_id_by_custom_id(consumer_id)
    url = os.environ.get("APIGATEWAY_ADMIN_INTERNAL_BASEURL") + "/consumers/" + str(gateway_consumer_id) + "/key-auth"

    payload = {}
    headers = {
        'Content-Type': "application/json",
        'Cache-Control': "no-cache"
    }

    response = requests.request("POST", url, data=json.dumps(payload), headers=headers)
    data = response.json()
    data["consumer"] = {"id": consumer_id}
    print("New key: ", data)

    return data


def get_consumer_name_by_consumer_id(consumerid):
    url = os.environ.get("APIGATEWAY_ADMIN_INTERNAL_BASEURL") + "/consumers/" + consumerid

    headers = {
        'Content-Type': "application/json",
        'Cache-Control': "no-cache"
    }

    response = requests.request("GET", url, headers=headers)

    return response.json()


def get_consumer_id_by_auth_key(authkey):
    url = os.environ.get("APIGATEWAY_ADMIN_INTERNAL_BASEURL") + "/key-auths"

    querystring = {"key": authkey}

    headers = {
        'Content-Type': "application/json",
        'Cache-Control': "no-cache"
    }

    response = requests.request("GET", url, headers=headers, params=querystring)
    return response.json().get("data", {}).get("consumer_id")


def get_consumer_id_by_basic_auth(login):
    url = os.environ.get("APIGATEWAY_ADMIN_INTERNAL_BASEURL") + "/basic-auths/" + login + "/consumer"

    headers = {
        'Content-Type': "application/json",
        'Cache-Control': "no-cache"
    }

    response = requests.request("GET", url, headers=headers)
    return response.json().get("custom_id", None)


def get_basic_auth_by_consumer_id(consumer_id):
    gateway_consumer_id = get_consumer_id_by_custom_id(consumer_id)
    url = os.environ.get("APIGATEWAY_ADMIN_INTERNAL_BASEURL") + "/consumers/" + gateway_consumer_id + "/basic-auth"
    #print(url)
    headers = {
        'Content-Type': "application/json",
        'Cache-Control': "no-cache"
    }

    response = requests.request("GET", url, headers=headers)

    return response.json().get("data", {})


def create_rabbitmq_vhost_for_consumer(consumer_name, username):
    return True


@app.after_request
def after_request(response):
    """ Logging after every request. """
    if request.full_path not in ["/metrics?", "/metrics", "/healthz", "/healthz?"]:
        ts = strftime('[%Y-%b-%d %H:%M]')
        data = {}
        data["account_id"] = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
        data["username"] = request.headers.get("X-Credential-Username", "UNKNOWN")
        data["consumername"] = request.headers.get("X-Consumer-Username", "UNKNOWN")
    # log(" |-| ".join([json.dumps(data), request.remote_addr, request.method, request.scheme, request.full_path,
    #                   response.status]))
    return response


@app.teardown_request
def teardown_request(exception):
    if random.random() < 0.05:
        print("Garbage-Collection triggered")
        threading.Thread(target=clean_up_old_guest_logins).start()


    if exception:
        Config.db_session.rollback()
    Config.db_session.remove()



if __name__ == '__main__':
    app.run(debug=True, port=8000)
